package testProject1;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class CompteTest {

    @Test
    public void testCreationCompte() {
        // Créer un compte bancaire
        CompteBancaire compte = new CompteBancaire();

        // Vérifier que le débit est à 0
        assertEquals(0.0, compte.getDebit(), 0.001);

        // Vérifier que le crédit est à 0
        assertEquals(0.0, compte.getCredit(), 0.001);
    }
    
    
    @Test
    public void testCrediterCompte() {
        // Créer un compte bancaire
        CompteBancaire compte = new CompteBancaire();

        // Créditer le compte de 100.0
        compte.crediter(100.0);

        // Vérifier que le crédit a été incrémenté de 100.0
        assertEquals(100.0, compte.getCredit(), 0.001);
    }
    
    @Test
    public void testDebiterCompte() {
        // Créer un compte bancaire
        CompteBancaire compte = new CompteBancaire();

        // Créditer le compte de 200.0
        compte.crediter(200.0);

        // Débiter le compte de 50.0
        compte.debiter(50.0);

        // Vérifier que le crédit est toujours à 200.0
        assertEquals(200.0, compte.getCredit(), 0.001);

        // Vérifier que le débit est égal à 50.0
        assertEquals(50.0, compte.getDebit(), 0.001);
    }
    
    
    @Test
    public void testCrediterAvecMontantNegatif() {
        // Créer un compte bancaire
        CompteBancaire compte = new CompteBancaire();

        // Tenter de créditer le compte avec un montant négatif
        compte.crediter(-50.0);

        // Vérifier que le crédit reste à 0.0
        assertEquals(0.0, compte.getCredit(), 0.001);
    }

    @Test
    public void testDebiterAvecMontantNegatif() {
        // Créer un compte bancaire
        CompteBancaire compte = new CompteBancaire();

        // Tenter de débiter le compte avec un montant négatif
        compte.debiter(-50.0);

        // Vérifier que le débit reste à 0.0
        assertEquals(0.0, compte.getDebit(), 0.001);
    }

    
    @Test
    public void testSoldeApresOperations() {
        // Créer un compte bancaire
        CompteBancaire compte = new CompteBancaire();

        // Créditer le compte de 100.0
        compte.crediter(100.0);

        // Débiter le compte de 50.0
        compte.debiter(50.0);

        // Créditer le compte de 30.0
        compte.crediter(30.0);

        // Débiter le compte de 20.0
        compte.debiter(20.0);

        // Vérifier que le solde est égal à 60.0 (100 - 50 + 30 - 20)
        assertEquals(60.0, compte.getSolde(), 0.001);
    }
    
    @Test
    public void testHistoriqueDebitsCredits() {
        // Créer un compte bancaire
        CompteBancaire compte = new CompteBancaire();

        // Créditer le compte de 100.0
        compte.crediter(100.0);

        // Débiter le compte de 50.0
        compte.debiter(50.0);

        // Créditer le compte de 30.0
        compte.crediter(30.0);

        // Débiter le compte de 20.0
        compte.debiter(20.0);

        // Vérifier l'historique des crédits
        double[] credits = compte.getHistoriqueCredits();
        assertEquals(100.0, credits[0], 0.001);
        assertEquals(30.0, credits[1], 0.001);

        // Vérifier l'historique des débits
        double[] debits = compte.getHistoriqueDebits();
        assertEquals(50.0, debits[0], 0.001);
        assertEquals(20.0, debits[1], 0.001);

        // Vérifier que le solde est égal à 60.0 (100 - 50 + 30 - 20)
        assertEquals(60.0, compte.getSolde(), 0.001);
    }
    
    
    
}

