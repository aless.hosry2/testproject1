package testProject1;

public class CompteBancaire {
    private double debit;
    private double credit;
    private double[] historiqueCredits;
    private double[] historiqueDebits;
    private int indexCredit;
    private int indexDebit;
    
    private int indexDebit3;
    private int indexDebit4;
    

    public CompteBancaire() {
        this.debit = 0.0;
        this.credit = 0.0;
        this.historiqueCredits = new double[10]; // Taille arbitraire du tableau
        this.historiqueDebits = new double[10];  // Taille arbitraire du tableau
        this.indexCredit = 0;
        this.indexDebit = 0;
    }

    public double getDebit() {
        return debit;
    }

    public double getCredit() {
        return credit;
    }

    public void crediter(double montant) {
        if (montant > 0) {
        	
        	if (indexCredit < historiqueCredits.length) {
                historiqueCredits[indexCredit++] = montant;
            } else {
                historiqueCredits[0] += montant;
            } // new apres amelioration 2
        	
            this.credit += montant;
        }
    }

    public void debiter(double montant) {
        if (montant > 0) {
        	
        	if (indexDebit < historiqueDebits.length) {
                historiqueDebits[indexDebit++] = montant;
            } else {
                historiqueDebits[0] += montant;
            } // new apres amelioration 2
        	
            this.debit += montant;
        }
    }
    
    public double getSolde() {
        return credit - debit;
    }
    
    public double[] getHistoriqueCredits() {
        return historiqueCredits;
    }

    public double[] getHistoriqueDebits() {
        return historiqueDebits;
    }
    
    
    
    
    
}

